dual_pairs
==========

A SageMath package for computing with dual pairs of algebras.


Author
------

Peter Bruin, <P.J.Bruin@math.leidenuniv.nl>


Prerequisites
-------------

- SageMath, <http://www.sagemath.org/>.  This package has been tested
  with SageMath versions from 8.2 up to 9.7; it may also work with
  older or newer versions.


Installation
------------

    $ sage --pip install dual_pairs

Documentation
-------------

The documentation for this package is automatically generated from the
source code and is available at
<https://dual-pairs.readthedocs.io/en/latest/>.

TODO
----

- Better documentation

- More functionality for torsors
