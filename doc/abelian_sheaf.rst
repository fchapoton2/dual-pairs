Sheaves of Abelian groups
=========================

.. automodule:: dual_pairs.abelian_sheaf
   :members:
