Class groups of finite flat algebras
====================================

.. automodule:: dual_pairs.class_group
   :members:
