Constructing a dual pair of algebras from a dihedral field
==========================================================

.. automodule:: dual_pairs.dual_pair_from_dihedral_field
   :members:
