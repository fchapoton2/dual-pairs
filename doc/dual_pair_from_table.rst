Constructing a dual pair of algebras from a table
=================================================

.. automodule:: dual_pairs.dual_pair_from_table
   :members:
