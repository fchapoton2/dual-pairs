Importing a dual pair of algebras from a file
=============================================

.. automodule:: dual_pairs.dual_pair_import
   :members:
