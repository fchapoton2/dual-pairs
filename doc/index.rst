.. Documentation index

The ``dual_pairs`` package
==========================

The `SageMath`_ package ``dual_pairs`` implements computations with
finite locally free group schemes and Galois representations in the
form of *dual pairs of algebras*.  See the preprint `Dual pairs of
algebras and finite commutative group schemes`_ for details.

.. _`SageMath`: http://www.sagemath.org/

.. _`Dual pairs of algebras and finite commutative group schemes`:
   https://arxiv.org/abs/1709.09847

Author
------

Peter Bruin <P.J.Bruin@math.leidenuniv.nl>

Installation
------------

The package can be installed using

    ``$ sage --pip install dual_pairs``

Source code
-----------

The source code is available at <https://gitlab.com/pbruin/dual-pairs>.


Documentation of the modules
============================

The documentation pages in the table below are automatically
generated from the source code.

Dual pairs of algebras
----------------------

.. toctree::
   :maxdepth: 2

   dual_pair
   dual_pair_from_dihedral_field
   dual_pair_from_table
   dual_pair_import
   dual_pair_rational

Finite flat algebras
--------------------

.. toctree::
   :maxdepth: 2

   finite_flat_algebra
   finite_flat_algebra_element
   finite_flat_algebra_module
   class_group
   unit_group
   selmer_group

Torsors and extension groups
----------------------------

.. toctree::
   :maxdepth: 2

   ext_group
   torsor_class_group
   torsor_pair
   abelian_sheaf
   simplicial_sheaf

Miscellaneous
-------------

.. toctree::
   :maxdepth: 2

   abelian_group_homomorphism
   extensions
   group_structure
   padic_roots
   smith_form

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
