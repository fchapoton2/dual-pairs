Utility functions for `p`-adic polynomials
==========================================

.. automodule:: dual_pairs.padic_roots
   :members:
