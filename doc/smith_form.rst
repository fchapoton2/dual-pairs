Smith normal form for Abelian groups
====================================

.. automodule:: dual_pairs.smith_form
   :members:
